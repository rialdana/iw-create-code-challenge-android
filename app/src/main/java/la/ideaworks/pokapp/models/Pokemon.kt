package la.ideaworks.pokapp.models

data class Pokemon(
    var id: Int,
    var name: String,
    var specie: ArrayList<String>,
    var weight: Int,
    var abilities: ArrayList<String>,
    var generation: String,
    var image: String
)