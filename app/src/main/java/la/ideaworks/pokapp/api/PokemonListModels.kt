package la.ideaworks.pokapp.api

/**
 *
 * Necessary classes to bring Pokemon names from API
 *
 */
data class PokemonListWrapper(
    var count: Int,
    var next: String,
    var previous: Boolean,
    var results: ArrayList<PokemonListResult>
)

data class PokemonListResult(var name: String, var url: String)

/**
 *
 * Necessary classes to bring Pokemon species from API
 *
 */

data class PokemonSpecieWrapper(
    var base_happiness: Int,
    var capture_rate: Int,
    var color: Color,
    var egg_groups: ArrayList<EggGroups>,
    var evolution_chain: EvolutionChain,
    var evolves_from_species: EvolvesFrom,
    var flavor_text_entries: ArrayList<FlavorTextEntries>,
    var form_descriptions: ArrayList<Any>,
    var forms_switchable: Boolean,
    var gender_rate: Int,
    var genera: ArrayList<Genera>,
    var generation: Generation,
    var growth_rate: GrowthRate,
    var habitat: Habitat,
    var has_gender_differences: Boolean,
    var hatch_counter: Int,
    var id: Int,
    var is_baby: Boolean,
    var name: String,
    var names: ArrayList<Names>,
    var order: Int,
    var pal_park_encounters: ArrayList<Any>,
    var pokedex_numbers: ArrayList<Any>,
    var shape: Shape,
    var varieties: ArrayList<Any>
)

data class EvolutionChain(var url: String)
data class Shape(var name: String, var url:String)
data class Names(var language: Language, var name: String)
data class Habitat(var name:String, var url:String)
data class GrowthRate(var name:String, var url:String)
data class Generation(var name:String, var url: String)
data class Genera (var genus: String, var language: Language)
data class EggGroups(var name: String, var url: String)
data class Color(var name: String, var url: String)
data class EvolvesFrom(var name: String, var url: String)
data class FlavorTextEntries(var flavor_text: String, var language: Language, var version: Version)
data class Language(var name: String, var url: String)

/**
 *
 * Necessary classes to bring Pokemon JSON objects from API
 *
 */
data class PokemonItemWrapper(
    var abilities: ArrayList<Abilities>,
    var base_experience: Int,
    var forms: ArrayList<Form>,
    var game_indices: ArrayList<GameIndice>,
    var height: Int,
    var held_items: ArrayList<Any>,
    var id: Int,
    var is_default: Boolean,
    var location_area_encounters: String,
    var moves: ArrayList<MoveWrapper>,
    var name: String,
    var order: Int,
    var species: Specie,
    var sprites: Sprites,
    var stats: ArrayList<Stats>,
    var types: ArrayList<Types>,
    var weight: Int
)

data class Types(var slot: Int, var type: Type)

data class Type(var name: String, var url: String)

data class Stats(var base_stat: Int, var effort: Int, var stat: Stat)

data class Stat(var name: String, var url: String)

data class Specie(var name: String, var url: String)

data class Sprites(
    var back_default: String,
    var back_female: String,
    var back_shiny: String,
    var back_shiny_female: String,
    var front_female: String,
    var front_shiny: String,
    var front_shiny_female: String
)

data class Abilities(
    var ability: Ability,
    var is_hidden: Boolean,
    var slot: Int
)

data class Ability(var name: String, var url: String)

data class Form(var name: String, var url: String)

data class GameIndice(var game_index: Int, var version: Version)

data class Version(var name: String, var url: String)

data class MoveWrapper(var move: Move, var version_group_details: ArrayList<VersionGroupDetail>)

data class Move(var name: String, var url: String)

data class VersionGroupDetail(
    var level_learned_at: Int,
    var move_learn_method: MoveLearnMethod,
    var version_group: VersionGroup
)

data class MoveLearnMethod(var name: String, var url: String)

data class VersionGroup(var name: String, var url: String)