package la.ideaworks.pokapp.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface PokemonApiService {
    @GET("pokemon/")
    fun getPokemonNameList() : Call<PokemonListWrapper>

    @GET("pokemon/{id}")
    fun getPokemonList(@Path("id") id : Int) : Call<PokemonItemWrapper>

    @GET("pokemon-species/{id}")
    fun getPokemonSpecie(@Path("id") id : Int) : Call<PokemonSpecieWrapper>
}