package la.ideaworks.pokapp

const val VIEWMODEL_EXCEPTION = "INVALID ACTIVITY"
const val API_CALL_SUCCESS = "SUCCESS"
const val API_CALL_LIST_LOADING = "LOADING_LIST"
const val API_CALL_ITEM_LOADING = "LOADING_ITEM"
const val API_CALL_ERROR = "ERROR"
const val POKEMON_LOG = "POKEMON_LOG"
const val POKEMON_DATABASE_NAME = "pokemon_database"
const val POKEMON_ADDED ="Añadido a tu colección"
const val SHARE_POKEMON_TITLE ="¡Compartiendo el pokemon!"