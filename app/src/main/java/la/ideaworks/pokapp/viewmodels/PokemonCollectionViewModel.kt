package la.ideaworks.pokapp.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import kotlinx.coroutines.launch
import la.ideaworks.pokapp.POKEMON_LOG
import la.ideaworks.pokapp.data.PokemonEntity
import la.ideaworks.pokapp.data.PokemonRepository
import la.ideaworks.pokapp.data.PokemonRoomDatabase

class PokemonCollectionViewModel (application: Application) : AndroidViewModel(application) {
    private val repository: PokemonRepository
    val allPokemons: LiveData<List<PokemonEntity>>
    val savedInRoom = MutableLiveData<Boolean?>()
    val selectedPokemon = MutableLiveData<PokemonEntity>()

    init {
        val pokemonDao = PokemonRoomDatabase.getDatabase(application, viewModelScope).pokemonDao()
        repository = PokemonRepository(pokemonDao)
        allPokemons = repository.allPokemons
    }

    fun verifyIfIdIsAdded(id: Int) = viewModelScope.launch{
        val id = repository.getPokemonStatus(id)
        if (id == 0){
            savedInRoom.postValue(false)
        }else{
            savedInRoom.postValue(true)
        }
    }

    fun insert(pokemon: PokemonEntity) = viewModelScope.launch {
        repository.insert(pokemon)
    }

    fun delete(pokemon: PokemonEntity) = viewModelScope.launch {
        repository.delete(pokemon)
    }
}
