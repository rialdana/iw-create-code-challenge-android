package la.ideaworks.pokapp.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import la.ideaworks.pokapp.*
import la.ideaworks.pokapp.api.ApiService
import la.ideaworks.pokapp.api.PokemonItemWrapper
import la.ideaworks.pokapp.api.PokemonListWrapper
import la.ideaworks.pokapp.api.PokemonSpecieWrapper
import la.ideaworks.pokapp.models.Pokemon
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PokemonListViewModel : ViewModel() {

    val pokemonsList = MutableLiveData<ArrayList<PokemonItemWrapper>?>()
    val selectedPokemon = MutableLiveData<PokemonItemWrapper?>()
    val pokemonItem = MutableLiveData<Pokemon?>()
    val apiCallStatus = MutableLiveData<String?>()

    init {
        pokemonsList.postValue(arrayListOf())
        selectedPokemon.postValue(null)
        apiCallStatus.postValue("")
        pokemonItem.postValue(null)
    }

    fun loadPokemonSpecieAndGen(pokemonObject: PokemonItemWrapper) {

        apiCallStatus.postValue(API_CALL_ITEM_LOADING)

        val specieID = extractSpecieID(pokemonObject.species.url)
        ApiService.create().getPokemonSpecie(specieID)
            .enqueue(object : Callback<PokemonSpecieWrapper> {
                override fun onFailure(call: Call<PokemonSpecieWrapper>, t: Throwable) {
                    apiCallStatus.postValue(API_CALL_ERROR)
                    Log.e(POKEMON_LOG, "ERROR BRINGING SPECIES", t)
                }

                override fun onResponse(
                    call: Call<PokemonSpecieWrapper>,
                    response: Response<PokemonSpecieWrapper>
                ) {
                    val specie = response.body()
                    val pokemonSpecies = arrayListOf<String>()
                    val pokemonAbilities = arrayListOf<String>()

                    specie!!.egg_groups.forEach { specie ->
                        pokemonSpecies.add(specie.name)
                    }

                    pokemonObject.abilities.forEach { pokemon ->
                        pokemonAbilities.add(pokemon.ability.name)
                    }

                    val pokemon = Pokemon(
                        pokemonObject.id,
                        pokemonObject.name,
                        pokemonSpecies,
                        pokemonObject.weight,
                        pokemonAbilities,
                        specie.generation.name,
                        pokemonObject.sprites.front_shiny
                    )

                    pokemonItem.postValue(pokemon)
                    apiCallStatus.postValue(API_CALL_SUCCESS)
                }
            })
    }

    fun loadPokemons() {
        apiCallStatus.postValue(API_CALL_LIST_LOADING)
        ApiService.create().getPokemonNameList().enqueue(object : Callback<PokemonListWrapper> {
            override fun onFailure(call: Call<PokemonListWrapper>, t: Throwable) {
                apiCallStatus.postValue(API_CALL_ERROR)
            }

            override fun onResponse(
                call: Call<PokemonListWrapper>,
                response: Response<PokemonListWrapper>
            ) {
                val listadoPokemon = response.body()
                extractPokemonID(listadoPokemon!!)
            }

        })
    }

    private fun extractPokemonID(listadoPokemon: PokemonListWrapper) {
        val pokemonIdList = arrayListOf<Int>()
        listadoPokemon.results.forEach { pokemonInfo ->
            var rawId = pokemonInfo.url.substring(34)
            var id = 0
            if ("/" in rawId) {
                rawId = rawId.replace("/", "").trim()
                id = rawId.toInt()
            }
            pokemonIdList.add(id)
            Log.i("POKEMON-ID", "$id")
        }
        loadPokemonsUsingId(pokemonIdList)
    }

    private fun loadPokemonsUsingId(pokemonIdList: ArrayList<Int>) {

        var pokemonIterationCounter = 0

        val pokemonObjectsList = arrayListOf<PokemonItemWrapper>()
        pokemonIdList.forEach { pokemonId ->
            ApiService.create().getPokemonList(pokemonId)
                .enqueue(object : Callback<PokemonItemWrapper> {
                    override fun onFailure(call: Call<PokemonItemWrapper>, t: Throwable) {
                        apiCallStatus.postValue(API_CALL_ERROR)
                    }

                    override fun onResponse(
                        call: Call<PokemonItemWrapper>,
                        response: Response<PokemonItemWrapper>
                    ) {
                        val pokemonItem = response.body()!!
                        pokemonObjectsList.add(pokemonItem)

                        pokemonIterationCounter++

                        // This "if" will confirm that all the objects have been added to the pokemon List
                        if (pokemonIterationCounter == pokemonIdList.size) {
                            val orderedPokemonList = arrayListOf<PokemonItemWrapper>()

                            // Since the list is not ordered by ID, due to the Async of the retrofit response
                            // this code is meant to order the Pokemons by ID and then send it to
                            // the viewModel
                            pokemonIdList.forEach { idFromList ->
                                pokemonObjectsList.forEach { idInObject ->
                                    if (idFromList == idInObject.id) orderedPokemonList.add(
                                        idInObject
                                    )
                                }
                            }
                            pokemonsList.postValue(orderedPokemonList)
                            apiCallStatus.postValue(API_CALL_SUCCESS)
                        }

                    }

                })
        }
    }

    private fun extractSpecieID(url: String): Int {
        var id = url.substring(42)

        if ("/" in id) id = id.replace("/", "").trim()

        return id.toInt()
    }

}