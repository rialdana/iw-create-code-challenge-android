package la.ideaworks.pokapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import la.ideaworks.pokapp.R
import la.ideaworks.pokapp.api.PokemonItemWrapper

class PokemonAdapter(var pokemonList: ArrayList<PokemonItemWrapper>, var listener: PokemonViewHolder.OnPokemonSelected) :
RecyclerView.Adapter<PokemonAdapter.PokemonViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_pokemon, parent, false)
        return PokemonViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return pokemonList.size
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        holder.bindData(pokemonList[position])
    }


    class PokemonViewHolder(itemView: View, var listener: OnPokemonSelected) : RecyclerView.ViewHolder(itemView){
        val picture = itemView.findViewById<ImageView>(R.id.item_pokemon_picture)
        val name = itemView.findViewById<TextView>(R.id.item_pokemon_name)
        val id = itemView.findViewById<TextView>(R.id.item_pokemon_id)
        val container = itemView.findViewById<View>(R.id.item_container)

        fun bindData(pokemon: PokemonItemWrapper){
            Glide.with(itemView).load(pokemon.sprites.front_shiny).into(picture)
            name.text = pokemon.name
            id.text = "#${pokemon.id}"
            container.setOnClickListener {
                listener.onPokemonClicked(pokemon)
            }
        }

        interface OnPokemonSelected{
            fun onPokemonClicked(pokemon: PokemonItemWrapper)
        }
    }
}
