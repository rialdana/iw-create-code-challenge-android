package la.ideaworks.pokapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import la.ideaworks.pokapp.R
import la.ideaworks.pokapp.data.PokemonEntity

class PokemonCollectionAdapter(
    var pokemons: List<PokemonEntity>,
    var listener: PokemonCollectionViewHolder.OnPokemonSelected
) : RecyclerView.Adapter<PokemonCollectionAdapter.PokemonCollectionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonCollectionViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_pokemon, parent, false)
        return PokemonCollectionViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return pokemons.size
    }

    override fun onBindViewHolder(holder: PokemonCollectionViewHolder, position: Int) {
        holder.bindData(pokemons[position])
    }


    class PokemonCollectionViewHolder(itemView: View, var listener: OnPokemonSelected) :
        RecyclerView.ViewHolder(itemView) {
        val picture = itemView.findViewById<ImageView>(R.id.item_pokemon_picture)
        val name = itemView.findViewById<TextView>(R.id.item_pokemon_name)
        val id = itemView.findViewById<TextView>(R.id.item_pokemon_id)
        val container = itemView.findViewById<View>(R.id.item_container)

        fun bindData(pokemon: PokemonEntity) {
            Glide.with(itemView).load(pokemon.image).into(picture)
            name.text = pokemon.name
            id.text = "#${pokemon.id}"
            container.setOnClickListener {
                listener.onPokemonClicked(pokemon)
            }
        }

        interface OnPokemonSelected {
            fun onPokemonClicked(pokemon: PokemonEntity)
        }
    }
}
