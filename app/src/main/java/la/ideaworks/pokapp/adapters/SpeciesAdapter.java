package la.ideaworks.pokapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import la.ideaworks.pokapp.R;

public class SpeciesAdapter extends RecyclerView.Adapter<SpeciesAdapter.SpeciesViewHolder> {
    private ArrayList<String> species;

    public SpeciesAdapter(ArrayList<String> species){
        this.species = species;
    }

    @NonNull
    @Override
    public SpeciesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_specie, parent, false);

        return new SpeciesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SpeciesViewHolder holder, int position) {
        holder.bindData(species.get(position));
    }

    @Override
    public int getItemCount() {
        return species.size();
    }


    class SpeciesViewHolder extends RecyclerView.ViewHolder {

        TextView specieTextView;

        SpeciesViewHolder(@NonNull View itemView) {
            super(itemView);
            specieTextView = itemView.findViewById(R.id.item_specie);
        }

        void bindData(String specie){
            specieTextView.setText(specie);
        }
    }
}

