package la.ideaworks.pokapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import la.ideaworks.pokapp.R;

public class AbilityAdapter extends RecyclerView.Adapter<AbilityAdapter.AbilitiesViewHolder> {
    private ArrayList<String> abilities;

    public AbilityAdapter(ArrayList<String> abilities) {
        this.abilities = abilities;
    }

    @NonNull
    @Override
    public AbilitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ability, parent, false);

        return new AbilitiesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AbilitiesViewHolder holder, int position) {
        holder.bindData(abilities.get(position));
    }

    @Override
    public int getItemCount() {
        return abilities.size();
    }

    class AbilitiesViewHolder extends RecyclerView.ViewHolder {

        TextView abilityTextView;

        AbilitiesViewHolder(@NonNull View itemView) {
            super(itemView);
            abilityTextView = itemView.findViewById(R.id.item_ability);
        }

        void bindData(String ability) {
            abilityTextView.setText(ability);
        }
    }
}
