package la.ideaworks.pokapp.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import la.ideaworks.pokapp.*
import la.ideaworks.pokapp.adapters.AbilityAdapter
import la.ideaworks.pokapp.adapters.PokemonAdapter
import la.ideaworks.pokapp.adapters.SpeciesAdapter
import la.ideaworks.pokapp.data.PokemonEntity

import la.ideaworks.pokapp.databinding.FragmentPokemonDetailBinding
import la.ideaworks.pokapp.viewmodels.PokemonCollectionViewModel
import la.ideaworks.pokapp.viewmodels.PokemonListViewModel

class PokemonDetailFragment : Fragment() {

    private lateinit var binding: FragmentPokemonDetailBinding
    private lateinit var viewModel: PokemonListViewModel
    private lateinit var viewModelCollection: PokemonCollectionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pokemon_detail, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = activity?.run {
            ViewModelProviders.of(this)[PokemonListViewModel::class.java]
        } ?: throw Exception(VIEWMODEL_EXCEPTION)

        viewModelCollection = activity?.run {
            ViewModelProviders.of(this)[PokemonCollectionViewModel::class.java]
        } ?: throw java.lang.Exception(VIEWMODEL_EXCEPTION)

        viewModel.apiCallStatus.observe(requireActivity(), Observer {status ->
            when (status){
                API_CALL_ITEM_LOADING -> {
                    binding.pokemonDetailLoading.visibility = View.VISIBLE
                }
                API_CALL_SUCCESS -> {
                    binding.pokemonDetailLoading.visibility = View.GONE
                }
                API_CALL_ERROR -> {
                    binding.pokemonDetailLoading.visibility = View.GONE
                }
            }

        })

        viewModel.pokemonItem.observe(requireActivity(), Observer {pokemon ->
            if (pokemon != null){
                binding.pokemonId.text = "#${pokemon.id}"
                binding.pokemonName.text = pokemon.name.capitalize()
                binding.pokemonGeneration.text = pokemon.generation.capitalize()
                binding.pokemonWeight.text = "Weight: ${pokemon.weight}"
                binding.pokemonSpecies.adapter = SpeciesAdapter(pokemon.specie)
                binding.pokemonAbilities.adapter = AbilityAdapter(pokemon.abilities)

                viewModelCollection.verifyIfIdIsAdded(pokemon.id)

                Glide.with(view).load(pokemon.image).into(binding.pokemonPicture)
            }
        })

        viewModelCollection.savedInRoom.observe(requireActivity(), Observer {
            if (it!!){
                binding.pokemonFavorite.hide()
            }else{
                binding.pokemonFavorite.show()
            }
        })

        binding.pokemonFavorite.setOnClickListener {
            val selectedPokemon = viewModel.pokemonItem.value
            val pokemon = PokemonEntity(
                selectedPokemon!!.id,
                selectedPokemon.name,
                removeCharacters(selectedPokemon.specie),
                selectedPokemon.weight,
                removeCharacters(selectedPokemon.abilities),
                selectedPokemon.generation,
                selectedPokemon.image
            )
            viewModelCollection.insert(pokemon)
            binding.pokemonFavorite.hide()
            showSnackbar(POKEMON_ADDED)
        }

        binding.pokemonShare.setOnClickListener {

            val pokemon = viewModel.pokemonItem.value

            val message = pokemon!!.name + " Es un pokemon poderoso de especie " + pokemon.specie +
                    " y posee las siguientes habilidades: " + pokemon.abilities

            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, message)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, SHARE_POKEMON_TITLE)
            startActivity(shareIntent)

        }

        binding.goBackButton.setOnClickListener {
            activity!!.onBackPressed()
        }

    }

    fun removeCharacters(array: ArrayList<String>) : String{
        var text = ""
        for (item in array){
            text = "$item,$text"
        }
        return text
    }

    fun showSnackbar(text: String){
        Snackbar.make(view!!, text, Snackbar.LENGTH_SHORT).show()
    }
}
