package la.ideaworks.pokapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import la.ideaworks.pokapp.R;
import la.ideaworks.pokapp.adapters.AbilityAdapter;
import la.ideaworks.pokapp.adapters.SpeciesAdapter;
import la.ideaworks.pokapp.data.PokemonEntity;
import la.ideaworks.pokapp.databinding.FragmentPokemonSavedDetailBinding;
import la.ideaworks.pokapp.viewmodels.PokemonCollectionViewModel;

public class PokemonSavedDetailFragment extends Fragment {

    FragmentPokemonSavedDetailBinding binding;
    PokemonCollectionViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pokemon_saved_detail, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = ViewModelProviders.of(requireActivity()).get(PokemonCollectionViewModel.class);

        viewModel.getSelectedPokemon().observe(getViewLifecycleOwner(), pokemonEntity -> {
            if (pokemonEntity != null){
                binding.savedPokemonId.setText("#" + pokemonEntity.getId());
                binding.savedPokemonName.setText(pokemonEntity.getName());
                binding.savedPokemonGeneration.setText(pokemonEntity.getGeneration());
                binding.savedPokemonWeight.setText("Weight: " +pokemonEntity.getWeight());

                ArrayList<String> speciesList = convertToArrayList(pokemonEntity.getSpecie());
                binding.savedPokemonSpecies.setAdapter(new SpeciesAdapter(speciesList));

                ArrayList<String> abilitiesList = convertToArrayList(pokemonEntity.getAbilities());
                binding.savedPokemonAbilities.setAdapter(new AbilityAdapter(abilitiesList));

                Glide.with(this)
                        .load(pokemonEntity.getImage())
                        .into(binding.savedPokemonPicture);
            }
        });

        binding.savedPokemonRemove.setOnClickListener( buttonView -> {
            viewModel.delete(viewModel.getSelectedPokemon().getValue());
            getActivity().onBackPressed();
        });

        binding.savedPokemonShare.setOnClickListener(buttonView -> {

            String title = "¡Mira este grandioso pokemon!";
            PokemonEntity pokemon = viewModel.getSelectedPokemon().getValue();

            String message = pokemon.getName() + " Es un pokemon poderoso de especie " + pokemon.getSpecie() +
                    " y posee las siguientes habilidades: " + pokemon.getAbilities();

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, message);
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, title);
            startActivity(shareIntent);

        });

        binding.goBackButton.setOnClickListener(buttomView -> {
            Objects.requireNonNull(getActivity()).onBackPressed();
        });
    }

    public ArrayList<String> convertToArrayList(String text){
        String[] wordsList = text.split(",");
        List<String> items = Arrays.asList(wordsList);

        return new ArrayList<>(items);
    }
}
