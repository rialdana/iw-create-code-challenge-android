package la.ideaworks.pokapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import la.ideaworks.pokapp.*
import la.ideaworks.pokapp.adapters.PokemonAdapter
import la.ideaworks.pokapp.api.PokemonItemWrapper
import la.ideaworks.pokapp.databinding.FragmentPokemonListBinding
import la.ideaworks.pokapp.viewmodels.PokemonListViewModel

class PokemonListFragment : Fragment(), PokemonAdapter.PokemonViewHolder.OnPokemonSelected {

    private lateinit var viewModel: PokemonListViewModel
    private lateinit var binding: FragmentPokemonListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pokemon_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = activity?.run {
            ViewModelProviders.of(this)[PokemonListViewModel::class.java]
        } ?: throw Exception(VIEWMODEL_EXCEPTION)

        viewModel.loadPokemons()

        binding.buttonReloadPokemonList.setOnClickListener {
            viewModel.loadPokemons()
        }

        viewModel.apiCallStatus.observe(requireActivity(), Observer {status ->
            when (status){
                API_CALL_LIST_LOADING -> {
                    binding.pokemonListLoading.visibility = View.VISIBLE
                    binding.pokemonNoConnection.visibility = View.GONE
                }
                API_CALL_SUCCESS -> {
                    binding.pokemonListLoading.visibility = View.GONE
                    binding.pokemonNoConnection.visibility = View.GONE
                }
                API_CALL_ERROR -> {
                    binding.pokemonListLoading.visibility = View.GONE
                    binding.pokemonNoConnection.visibility = View.VISIBLE
                }
            }

        })

        viewModel.pokemonsList.observe(requireActivity(), Observer {
            if (it!!.size > 0){
                val adapter = PokemonAdapter(it, this)
                binding.pokemonList.adapter = adapter
            }
        })
    }

    override fun onPokemonClicked(pokemon: PokemonItemWrapper) {
        viewModel.loadPokemonSpecieAndGen(pokemon)
        Navigation.findNavController(view!!).navigate(R.id.action_poke_list_to_poke_detail)
    }

}
