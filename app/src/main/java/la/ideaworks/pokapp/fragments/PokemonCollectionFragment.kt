package la.ideaworks.pokapp.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import la.ideaworks.pokapp.POKEMON_LOG
import la.ideaworks.pokapp.R
import la.ideaworks.pokapp.VIEWMODEL_EXCEPTION
import la.ideaworks.pokapp.adapters.PokemonCollectionAdapter
import la.ideaworks.pokapp.data.PokemonEntity
import la.ideaworks.pokapp.databinding.FragmentPokemonCollectionBinding
import la.ideaworks.pokapp.viewmodels.PokemonCollectionViewModel

class PokemonCollectionFragment : Fragment(), PokemonCollectionAdapter.PokemonCollectionViewHolder.OnPokemonSelected {

    private lateinit var viewModel: PokemonCollectionViewModel
    private lateinit var binding: FragmentPokemonCollectionBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pokemon_collection, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = activity?.run {
            ViewModelProviders.of(this)[PokemonCollectionViewModel::class.java]
        } ?: throw Exception(VIEWMODEL_EXCEPTION)

        viewModel.allPokemons.observe(requireActivity(), Observer {
            val adapter = PokemonCollectionAdapter(it, this)
            binding.pokemonCollection.adapter = adapter

            if (it.isEmpty()){
                binding.noPokemonSaved.visibility = View.VISIBLE
            }else{
                binding.noPokemonSaved.visibility = View.GONE
            }

        })
    }

    override fun onPokemonClicked(pokemon: PokemonEntity) {
        viewModel.selectedPokemon.value = pokemon
        Navigation.findNavController(view!!).navigate(R.id.action_poke_collection_to_poke_saved_detail)
    }
}
