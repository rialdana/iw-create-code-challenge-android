package la.ideaworks.pokapp.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PokemonDao{
    @Query("SELECT * from pokemon_collection_table ORDER BY id ASC")
    fun getOrderedPokemons(): LiveData<List<PokemonEntity>>

    @Query("SELECT COUNT(id) FROM pokemon_collection_table WHERE id LIKE :pokemonId LIMIT 1")
    suspend fun getPokemonID(pokemonId: Int) : Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(pokemon: PokemonEntity)

    @Delete
    suspend fun deletePokemon(pokemon: PokemonEntity)
}