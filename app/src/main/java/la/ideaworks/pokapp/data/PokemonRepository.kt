package la.ideaworks.pokapp.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import la.ideaworks.pokapp.POKEMON_LOG

class PokemonRepository (private val pokemonDao: PokemonDao) {

    val allPokemons: LiveData<List<PokemonEntity>> = pokemonDao.getOrderedPokemons()
    lateinit var pokemonName: MutableLiveData<Int>

    suspend fun getPokemonStatus(pokemonId: Int) : Int{
        val result = pokemonDao.getPokemonID(pokemonId)

        if (result != null){
            Log.i(POKEMON_LOG, "ID: $result")
            return result
        }else{
            Log.i(POKEMON_LOG, "POKEMON NOT IN THE LIST: $result")
            return -1
        }


    }

    suspend fun insert(pokemon: PokemonEntity){
        pokemonDao.insert(pokemon)
    }

    suspend fun delete(pokemon: PokemonEntity){
        pokemonDao.deletePokemon(pokemon)
    }

}