package la.ideaworks.pokapp.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemon_collection_table")
data class PokemonEntity(
    @PrimaryKey @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "specie") val specie: String,
    @ColumnInfo(name = "weight") val weight: Int,
    @ColumnInfo(name = "abilities") val abilities: String,
    @ColumnInfo(name = "generation") val generation: String,
    @ColumnInfo(name = "image") val image: String
)