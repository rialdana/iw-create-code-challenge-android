# ¡Poke App!

## Descripción

Este proyecto ha sido creado para mostrar habilidades de creación de aplicaciones moviles para android. Para la compañía IdeaWorks

## Proceso de instalación

#### Paso 1
Descarga el APK desde el siguiente enlace: [poke_app.apk](/uploads/17d49a78a0bafd99a02f033e62078692/poke_app.apk)

#### Paso 2
Instala el APK en tu teléfono, en caso de recibir algun aviso pidiendote que habilites permisos para instalar una app de origen desconocido lo puedes lograr de la siguiente manera:

![apk-origenes-desconocidos](/uploads/516f88924502a727339b6791b94e5ff5/apk-origenes-desconocidos.jpg)

#### Paso 3
Busca una pokebola en tus apps y abrela, disfruta las funcionalidades

###### Lista general de pokemon
Aqui puedes ver el listado general de pokemons que esta siendo consultado desde https://pokeapi.co/ , al seleccionar un pokemon puedes ver sus detalles y guardarlo en tu coleccion al darle click en el botón con un corazon

![listado](/uploads/744aafd393f665656c6740cc9caa1a37/listado.jpg)

###### Colección de pokemon

Aqui puedes ver los pokemon que has guardado previamente, al ver el detalle de un pokemon lo puedes remover de tus favoritos al darle click al icono de basura

![collection](/uploads/064e4307097265c735695c4d9cbeba48/collection.jpg)